section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, rsp
    dec rsp
    mov byte[rsp], 0
    mov r9, 10
    .loop:
        dec rsp
        xor rdx, rdx
        div r9
        add rdx, '0'
        mov byte[rsp], dl
        cmp rax, 0
        jne .loop
        mov rdi, rsp
        push r8
        call print_string
        pop rsp
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    js .negative
    call print_uint
    jmp .end
    .negative:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        call print_uint
    .end:
       ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
    .loop:
        mov dl, byte[rdi+r8]
        mov cl, byte[rsi+r8]
        inc r8
        cmp dl, cl
        jnz .not_equals
        cmp dl, 0
        jnz .loop
        jmp .equals
    .not_equals:
        xor rax, rax
        ret
    .equals:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rdx, 1
    mov rdi, 0
    mov rsi, rsp
    syscall
    cmp rax, 0
    je .error
    cmp rax, -1
    je .error
    jmp .end
    .error:
        xor rax, rax
        pop r9
        ret
    .end:
        pop rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r15
    push r14
    push r13
    mov r14, rsi
    mov r13, rdi
    .check_symbols:
        call read_char
        cmp rax, 0x20
        je .check_symbols
        cmp rax, 0x9
        je .check_symbols
        cmp rax, 0xA
        je .check_symbols
        cmp rax, 0
        je .fail
        jmp .write
    .read:
        call read_char
        cmp rax, 0
        je .success
        cmp rax, 0x20
        je .success
        cmp rax, 0x9
        je .success
        cmp rax, 0xA
        je .success
    .write:
        mov byte[r13+r15], al
        inc r15
        cmp r15, r14
        je .fail
        jmp .read
    .success:
        mov byte[r13+r15], 0
        mov rax, r13
        mov rdx, r15
        jmp .end
    .fail:
        xor rax, rax
        xor rdx, rdx
    .end:
        pop r13
        pop r14
        pop r15
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    .loop:
        mov cl, byte[rdi]
        cmp rcx, 0
        je .end
        cmp rcx, '0'
        jb .end
        cmp rcx, '9'
        ja .end
        sub rcx, '0'
        imul rax, 10
        add rax, rcx
        inc rdi
        inc rdx
        jmp .loop
    .end:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    mov cl, byte[rdi]
    cmp rcx, '-'
    je .negative
    cmp rcx, '+'
    jne parse_uint
    inc rdi
    call parse_uint
    jmp .end
    .negative:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
    .end:
         ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        .loop:
            cmp byte[rdi + r8], 0
            je .success
            cmp rdx, r8
            je .fail
            mov cl, byte[rdi + r8]
            mov byte[rsi + r8], cl
            inc r8
            jmp .loop
        .success:
            mov byte[rsi + r8], 0
            mov rax, r8
            jmp .end
        .fail:
            xor rax, rax
        .end:
            ret
